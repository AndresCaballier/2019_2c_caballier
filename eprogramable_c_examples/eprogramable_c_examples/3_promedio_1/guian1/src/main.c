/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare cuatro variables sin signo de 8 bits y,
 *Declare  utilizando máscaras, rotaciones y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
Realice el mismo ejercicio, utilizando la definición de una “union”.
 *
 */


/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{
uint32_t Variable;
uint8_t Var1;
uint8_t Var2;
uint8_t Var3;
uint8_t Var4;

	Variable=0x01020304;

Var1= Variable;/* bits menos significativos */
Var2= Variable>>8;/* bits mas significativos */
//printf("  Var1\r\n es %d",Var1);

//printf("Var2\r\n es %d",Var2);
Var3= Variable>>16;/* se repite el proceso*/
Var4= Variable>>24;
//printf("Var3\r\n es %d",Var3);
//printf("Var4\r\n es %d",Var4);

/* lo mismo que lo anterior pero con "union"*/

union uniendo{
	struct {
		uint8_t Var5;
		uint8_t Var6;
		uint8_t Var7;
		uint8_t Var8;
	}prueba;

uint32_t unicaVariable;

}unir;


unir.unicaVariable=0x01020304;
//unicaVariable.Var5=Variable;
//unicaVariable.Var6=Variable>>8;
//unicaVariable.Var7=Variable>>16;
//unicaVariable.Var8=Variable>>24;

printf("Var5 es %d \r\n",unir.prueba.Var5);
printf("Var6 es %d \r\n",unir.prueba.Var6);
printf("Var7 es %d \r\n",unir.prueba.Var7);
printf("Var8 es %d \r\n",unir.prueba.Var8);




   // while(1)
    {
    }
	return 0;
}

/*==================[end of file]============================================*/

