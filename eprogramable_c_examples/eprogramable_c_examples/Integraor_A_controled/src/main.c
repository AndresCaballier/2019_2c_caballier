/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Integraor_A_controled/inc/main.h"

#include <stdio.h>
#define ON 1
#define OFF 0
#define TOOGLE 2
/*==================[macros and definitions]=================================*/
   typedef struct
   {
	   uint8_t n_led;/* inicia el numero de led a comparar*/
	   uint8_t n_ciclos;/* inicia la cantidad de ciclos de encendido /apagado*/
	   uint8_t n_periodo;/*inicia el tiempo de cada ciclo */
	   uint8_t n_modo;/*ON, OFF,TOGGLE */
   }mi_leds;
   //const int ON=1;
   //const int OFF=0;
   //const int TOOGLE=7;
   const int retardo=100;
/*==================[internal functions declaration]=========================*/
void controldeled(mi_leds *led){
uint8_t j=0;
uint8_t i=0;
	if(led->n_modo==ON){
	switch(led->n_led){
	case 1: printf("Enciendo led1 r\n"); break;
	case 2: printf("Enciendo led2 r\n"); break;
	case 3: printf("Enciendo led3 r\n");break;
	default:  printf("NO se encendio ninguno r\n");}


	}
	else{if(led->n_modo==OFF){
		switch(led->n_led){
		case 1: printf("apago led1 r\n");break;
		case 2: printf("apago led2 r\n");break;
		case 3: printf("apago led3 r\n");break;
		default:  printf("NO se apago ninguno r\n");}
	}


	else{
		if(led->n_modo==TOOGLE){
		while(i<led->n_ciclos){
		switch(led->n_led){
				case 1: printf("parpadeo led1 r\n");break;
				case 2: printf("parpadeo led2 r\n");break;
				case 3: printf("parpadeo led3 r\n");break;
				default:  printf("FIN r\n");}
		i++;
		while(j<retardo){
			j++;
		}

	}}}

}
	}

int main(void)

{			mi_leds LED;
	        LED.n_led=2;
		   LED.n_ciclos=2;
		   LED.n_modo=TOOGLE;

	controldeled(&LED);


	return 0;
}

/*==================[end of file]============================================*/

