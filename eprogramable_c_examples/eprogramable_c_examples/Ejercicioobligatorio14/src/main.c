/*
 * CÃ¡tedra: ElectrÃ³nica Programable
 * FIUNER - 2018
 * Autor/es:
 *
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
struct alumno {
	char nombres[12];
	char apellidos[20];
	int edad;
};

int main(void)
{
	struct alumno A1;
	char aux_nom[]="Juan";
	char aux_ape[]="Peres";

    strcpy(A1.nombres,aux_nom);
    strcpy(A1.apellidos,aux_ape);
    A1.edad=25;
    printf("%s A1.nombres \r\n", A1.nombres);
    printf("%s A1.apellidos \r\n", A1.apellidos);
    printf("%d A1.edad \r\n", A1.edad);

    struct alumno *A2;
     char aux_nom2[]="Andres";
     char aux_ape2[]="Caballier";
     strcpy(A2->nombres,aux_nom2);
     printf("%s A2.nombres \r\n", A2->nombres);
     strcpy(A2->apellidos,aux_ape2);
     printf("%s A2.apellidoss \r\n", A2->apellidos);
    A2->edad=28;
    printf("%d A2.edad \r\n", A2->edad);
	return 0;
}

/*==================[end of file]============================================*/

