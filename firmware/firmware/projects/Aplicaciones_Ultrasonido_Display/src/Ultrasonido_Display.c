/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include "../inc/Ultrasonido_Display.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "switch.h"
#include "led.h"


 /*==================[macros and definitions]=================================*/

 /*==================[external data definition]===============================*/

 /*==================[external functions definition]==========================*/

bool Key_aux_Tec1=false;
bool Key_aux_Tec2=false;
bool Key_aux_Tec3=false;
bool Key_aux_Tec4=false;
bool Distance_cm_bool=false;
bool Distance_inches_bool=false;
bool hold_cm=false;
bool hold_in=false;
int16_t Distance_cm;
int16_t Distance_inches;
 int main(void){
	int16_t distance_cm;
	int16_t distance_inches;
	LedsInit();
	int8_t Value_read=0;
 	SystemClockInit();
	uint16_t valor=0;
 	gpio_t pins[7];
 	SystemClockInit();
 	pins[0]=GPIO5;
 	pins[6]=LCD4;
 	bool Display=ITSE0803Init(pins);
 	SwitchesInit();//inicializa el drivers tecla
	int8_t Tecla=SwitchesRead();//lee la tecla
	int8_t Key;
 	int16_t Hold_cm=0, Hold_inches=0;//tecla para manetener el valor
 	bool Distance;
 	while (1){
	switch (Tecla)
	{case TEC_1: HcSr04Init(T_FIL2, T_FIL3);//llamo la funcion para que conexione ECHO con T_FIL, y TRIGGER con T_FIL3
		 if(Key_aux_Tec1==true)//Está apretada una tecla?
		 {
				HcSr04Init(T_FIL2, T_FIL3);//llamo la funcion para que conexione ECHO con T_FIL, y TRIGGER con T_FIL3
				LedOn(LED_RGB_G);
		 }
		 /*else if(Key_aux_Tec3==TRUE)
+		 {
+			 Distance_cm=HcSr04ReadDistanceCentimeters();//llamo la función que me devuelve la distancia en cm
+			 Distance_cm_bool=ITSE0803DisplayValue(Distance_cm);//muestro en display
+			 hold_cm=TRUE;
+		 }
+			 if(Key_aux_Tec4==TRUE)
+			 {
+			 Distance_inches=HcSr04ReadDistanceInches();//llamo la función que me devuelve la distancia en pulgadas
+			 Distance_inches_bool=ITSE0803DisplayValue(Distance_inches);//muestro en display
+			 hold_inches=TRUE;
+			 }
+			 if (Key_aux_Tec2==TRUE)
+			 {
+				 if(hold_cm==TRUE){
+					 ITSE0803DisplayValue(Distance_cm);
+			 }
+				 else if (hold_inches==TRUE){
+					 ITSE0803DisplayValue(Distance_inches);
+				 }
+			 }*/
+    Key=SwitchesRead();//lee la tecla
	switch (Key)
	{case SWITCH_1: {//Acvtivo y desactivo la medición
		Key_aux_Tec1=true;

	}
 	break;
	case TEC_2: Hold_cm=HcSr04ReadDistanceCentimeters();//llamo la función que me devuelve la distancia en cm
				Hold_inches=HcSr04ReadDistanceInches();//llamo la función que me devuelve la distancia en pulgadas
	case SWITCH_2: {
		Key_aux_Tec2=true;
	}
 	break;
	case TEC_3: HcSr04ReadDistanceCentimeters();
	case SWITCH_3:{
		Key_aux_Tec3=true;
	}
 	break;
    case TEC_4: HcSr04ReadDistanceInches();
	case SWITCH_4:{
		Key_aux_Tec4=true;
	}
 	break;
 	}

 	}
if(Distance==ITSE0803DisplayValue(distance_cm)){

	return 0;
}

/*if(Distance==ITSE0803DisplayValue(distance_cm)){
 	ITSE0803ReadValue();//devuelve el valor que lee
-	DelayMs(50);
+	DelayMs(50);//para que espere 50 ms cuando se apreta una tecla
 	Varaiable_aux=//para que cambie verdadero o falso
 }
 	else {
 		ITSE0803DisplayValue(distance_inches);
 		ITSE0803ReadValue();
-		DelayMs(50);
+		DelayMs(50);//para que espere 50 ms cuando se apreta una tecla
 	}
 	return 0;
-}
Add a comment to this line
+}*/
